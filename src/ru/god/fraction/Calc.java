package ru.god.fraction;

class Calc {
    static Fraction add(Fraction f1, Fraction f2) {
        int commonDenominator = commonDenominator(f1, f2);
        return reduction(new Fraction(f1.getNumerator() * (commonDenominator / f1.getDenominator()) +
                f2.getNumerator() * (commonDenominator / f2.getDenominator()), commonDenominator));
    }

    static Fraction residual(Fraction f1, Fraction f2) {
        int commonDenominator = commonDenominator(f1, f2);
        return reduction(new Fraction(f1.getNumerator() * (commonDenominator / f1.getDenominator()) -
                f2.getNumerator() * (commonDenominator / f2.getDenominator()), commonDenominator));
    }

    static Fraction multiplication(Fraction f1, Fraction f2) {
        return reduction(new Fraction(f1.getNumerator() * f2.getNumerator(), f1.getDenominator()
                * f2.getDenominator()));
    }

    static Fraction division(Fraction f1, Fraction f2) {
        return reduction(multiplication(f1, reverse(f2)));
    }

    /**
     * Метод, позволяющий определить общий знаменатель двух дробей
     *
     * @param f1 - первая дробь
     * @param f2 - вторая дробь
     * @return общий знаменатель
     */
    private static int commonDenominator(Fraction f1, Fraction f2) {
        f1 = reduction(f1);
        f2 = reduction(f2);
        int max = Math.max(f1.getDenominator(), f2.getDenominator());
        int min = Math.min(f1.getDenominator(), f2.getDenominator());
        for (int i = 1; ; i++) {
            if ((max * i) % min == 0) {
                return max * i;
            }
        }
    }

    /**
     * Метод, позволяющий сократить дробь
     *
     * @param f - исходная дробь
     * @return сокращенная дробь
     */
    private static Fraction reduction(Fraction f) {
        int numerator = Math.abs(f.getNumerator());
        int denominator = Math.abs(f.getDenominator());
        int min = Math.min(numerator, denominator);
        int max = Math.max(numerator, denominator);
        for (int i = Math.abs(min); i > 1; i--) {
            if ((min % i == 0) && (max % i == 0)) {
                return new Fraction(f.getNumerator() / i, f.getDenominator() / i);
            }
        }
        return f;
    }

    /**
     * Метод, позволяющий "первернуть" дробь, получить обратную
     *
     * @param f - исходная дробь
     * @return "перевернутая" дробь
     */
    private static Fraction reverse(Fraction f) {
        return new Fraction(f.getDenominator(), f.getNumerator());
    }
}
