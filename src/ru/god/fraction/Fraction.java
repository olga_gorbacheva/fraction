package ru.god.fraction;

public class Fraction {
    /**
     * Числитель
     */
    private int numerator;
    /**
     * Знаменатель
     */
    private int denominator;

    Fraction(int numerator, int denominator) {
        this.numerator = numerator * (denominator < 0 ? -1 : 1);
        this.denominator = Math.abs(denominator);
    }

    Fraction(int wholePart, int numerator, int denominator) {
        this(wholePart * (numerator < 0 ? -1 : 1) * denominator + numerator, denominator);
    }

    /**
     * Конструктор для преобразования целого числа в дробь
     *
     * @param number - целое число
     */
    Fraction(int number) {
        this(number, 1);
    }

    /**
     * Конструктор по умолчанию
     */
    Fraction() {
        this(0, 1);
    }

    int getNumerator() {
        return numerator;
    }

    int getDenominator() {
        return denominator;
    }

    @Override
    public String toString() {
        if ((denominator == 1) || (numerator == denominator)) {
            return String.valueOf(numerator / denominator);
        }
        if (Math.abs(numerator) > denominator) {
            return String.valueOf(numerator / denominator + "(" + Math.abs(numerator % denominator) + "/" + denominator + ")");
        }
        return String.valueOf(numerator + "/" + denominator);
    }
}
