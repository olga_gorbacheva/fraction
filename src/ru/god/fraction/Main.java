package ru.god.fraction;

public class Main {
    public static void main(String[] args) {
        Fraction f1 = new Fraction(3, 4);
        Fraction f2 = new Fraction(2, -1, 3);
        Fraction f3 = new Fraction(6);
        Fraction f4 = new Fraction(2);
        System.out.println(Calc.add(f1, f2));
        System.out.println(Calc.residual(f2, f1));
        System.out.println(Calc.multiplication(f3, f1));
        System.out.println(Calc.division(f1, f4));
    }
}
